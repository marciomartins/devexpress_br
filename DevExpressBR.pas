unit DevExpressBR;

interface

uses cxClasses, cxGridStrs;

procedure SetResorce;

implementation

procedure SetResorce;
begin
  //cxGrid
  cxSetResourceString(@scxGridRecursiveLevels, 'Voc� n�o pode criar n�veis recusivos');
  
  {
   cxSetResourceString(@,'');
   scxGridRecursiveLevels = 'You cannot create recursive levels';

  scxGridDeletingFocusedConfirmationText = 'Delete record?';
  scxGridDeletingSelectedConfirmationText = 'Delete all selected records?';

  scxGridNoDataInfoText = '<No data to display>';

  scxGridFilterRowInfoText = 'Click here to define a filter';
  scxGridNewItemRowInfoText = 'Click here to add a new row';

  scxGridFilterIsEmpty = '<Filter is Empty>';

  scxGridCustomizationFormCaption = 'Customization';
  scxGridCustomizationFormColumnsPageCaption = 'Columns';
  scxGridGroupByBoxCaption = 'Drag a column header here to group by that column';
  scxGridFilterApplyButtonCaption = 'Apply Filter';
  scxGridFilterCustomizeButtonCaption = 'Customize...';
  scxGridColumnsQuickCustomizationHint = 'Click here to show/hide/move columns';

  scxGridCustomizationFormBandsPageCaption = 'Bands';
  scxGridBandsQuickCustomizationHint = 'Click here to show/hide/move bands';

  scxGridCustomizationFormRowsPageCaption = 'Rows';

  scxGridConverterIntermediaryMissing = 'Missing an intermediary component!'#13#10'Please add a %s component to the form.';
  scxGridConverterNotExistGrid = 'cxGrid does not exist';
  scxGridConverterNotExistComponent = 'Component does not exist';
  scxImportErrorCaption = 'Import error';

  scxNotExistGridView = 'Grid view does not exist';
  scxNotExistGridLevel = 'Active grid level does not exist';
  scxCantCreateExportOutputFile = 'Can''t create the export output file';

  cxSEditRepositoryExtLookupComboBoxItem = 'ExtLookupComboBox|Represents an ultra-advanced lookup using the QuantumGrid as its drop down control';

  // date ranges

  scxGridYesterday = 'Yesterday';
  scxGridToday = 'Today';
  scxGridTomorrow = 'Tomorrow';
  scxGridLast30Days = 'Last 30 days';
  scxGridLast14Days = 'Last 14 days';
  scxGridLast7Days = 'Last 7 days';
  scxGridNext7Days = 'Next 7 days';
  scxGridNext14Days = 'Next 14 days';
  scxGridNext30Days = 'Next 30 days';
  scxGridLastTwoWeeks = 'Last two weeks';
  scxGridLastWeek = 'Last week';
  scxGridThisWeek = 'This week';
  scxGridNextWeek = 'Next week';
  scxGridNextTwoWeeks = 'Next two weeks';
  scxGridLastMonth = 'Last month';
  scxGridThisMonth = 'This month';
  scxGridNextMonth = 'Next month';
  scxGridLastYear = 'Last year';
  scxGridThisYear = 'This year';
  scxGridNextYear = 'Next year';
  scxGridPast = 'Past';
  scxGridFuture = 'Future';

  scxGridMonthFormat = 'mmmm yyyy';
  scxGridYearFormat = 'yyyy';

  // ChartView

  scxGridChartCategoriesDisplayText = 'Data';

  scxGridChartValueHintFormat = '%s for %s is %s';  // series display text, category, value
  scxGridChartPercentValueTickMarkLabelFormat = '0%';

  scxGridChartToolBoxDataLevels = 'Data Levels:';
  scxGridChartToolBoxDataLevelSelectValue = 'select value';
  scxGridChartToolBoxCustomizeButtonCaption = 'Customize Chart';

  scxGridChartNoneDiagramDisplayText = 'No diagram';
  scxGridChartColumnDiagramDisplayText = 'Column diagram';
  scxGridChartBarDiagramDisplayText = 'Bar diagram';
  scxGridChartLineDiagramDisplayText = 'Line diagram';
  scxGridChartAreaDiagramDisplayText = 'Area diagram';
  scxGridChartPieDiagramDisplayText = 'Pie diagram';
  scxGridChartStackedBarDiagramDisplayText = 'Stacked Bars diagram';
  scxGridChartStackedColumnDiagramDisplayText = 'Stacked Columns diagram';
  scxGridChartStackedAreaDiagramDisplayText = 'Stacked Area diagram';

  scxGridChartCustomizationFormSeriesPageCaption = 'Series';
  scxGridChartCustomizationFormSortBySeries = 'Sort by:';
  scxGridChartCustomizationFormNoSortedSeries = '<none series>';
  scxGridChartCustomizationFormDataGroupsPageCaption = 'Data Groups';
  scxGridChartCustomizationFormOptionsPageCaption = 'Options';

  scxGridChartLegend = 'Legend';
  scxGridChartLegendKeyBorder = 'Key Border';
  scxGridChartPosition = 'Position';
  scxGridChartPositionDefault = 'Default';
  scxGridChartPositionNone = 'None';
  scxGridChartPositionLeft = 'Left';
  scxGridChartPositionTop = 'Top';
  scxGridChartPositionRight = 'Right';
  scxGridChartPositionBottom = 'Bottom';
  scxGridChartAlignment = 'Alignment';
  scxGridChartAlignmentDefault = 'Default';
  scxGridChartAlignmentStart = 'Start';
  scxGridChartAlignmentCenter = 'Center';
  scxGridChartAlignmentEnd = 'End';
  scxGridChartOrientation = 'Orientation';
  scxGridChartOrientationDefault = 'Default';
  scxGridChartOrientationHorizontal = 'Horizontal';
  scxGridChartOrientationVertical = 'Vertical';
  scxGridChartBorder = 'Border';
  scxGridChartTitle = 'Title';
  scxGridChartToolBox = 'ToolBox';
  scxGridChartDiagramSelector = 'Diagram Selector';
  scxGridChartOther = 'Other';
  scxGridChartValueHints = 'Value Hints';

  scxGridLayoutViewCustomizeFormOk = 'OK';
  scxGridLayoutViewCustomizeFormCancel = 'Cancel';
  scxGridLayoutViewCustomizeFormApply = 'Apply';
  scxGridLayoutViewCustomizeWarningDialogCaption = 'Warning';
  scxGridLayoutViewCustomizeWarningDialogMessage = 'The layout has been changed. Do you want to save changes?';
  scxGridLayoutViewCustomizeLayoutButtonCaption = 'Layout Editor';
  scxGridLayoutViewCustomizeFormTemplateCard = 'Template Card';
  scxGridLayoutViewCustomizeFormViewLayout = 'View Layout';
  scxGridLayoutViewRecordCaptionDefaultMask = '[RecordIndex] of [RecordCount]';

  scxGridLockedStateImageText = 'Please wait...';
}

end;

initialization
  SetResorce;

end.
